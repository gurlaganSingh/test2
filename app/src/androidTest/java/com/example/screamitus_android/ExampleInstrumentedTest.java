package com.example.screamitus_android;



import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.screamitus_android", appContext.getPackageName());
    }

    @Rule
  public ActivityTestRule activityRule =
        new ActivityTestRule<>(MainActivity.class);
    // TC1:
           @Test public void TC1() throws InterruptedException {

               //getting the testbox and
               onView(withId(R.id.daysTextBox)).check(matches(isDisplayed()));
                onView(withId(R.id.resultsLabel)).check(matches((isDisplayed())));
               onView(withText("Calculate")).check(matches(isDisplayed()));


                            }

    //TC2
   @Test
   public void TC2() throws InterruptedException {
               Infection infection = new Infection();

                       onView(withId(R.id.daysTextBox))
                                .perform(typeText("9"));
                       onView(withText("CALCULATE")).perform(click());
                    String Output = "51 instructors infected";
                        onView(withId(R.id.resultsLabel))
                                .check(matches(withText(Output)));
          }

}
